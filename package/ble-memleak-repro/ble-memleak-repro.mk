# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 FORT Robotics Inc.

BLE_MEMLEAK_REPRO_LICENSE = GPL-2.0+

BLE_MEMLEAK_REPRO_DEPENDENCIES = bluez5_utils

define BLE_MEMLEAK_REPRO_EXTRACT_CMDS
	cp $(BLE_MEMLEAK_REPRO_PKGDIR)/ble-memleak-repro.c $(@D)
endef

define BLE_MEMLEAK_REPRO_BUILD_CMDS
	$(TARGET_CC) $(TARGET_CFLAGS) $(@D)/ble-memleak-repro.c -o $(@D)/ble-memleak-repro
endef

define BLE_MEMLEAK_REPRO_INSTALL_TARGET_CMDS
	install -D -m 0755 $(@D)/ble-memleak-repro $(TARGET_DIR)/usr/bin/ble-memleak-repro
endef

$(eval $(generic-package))
