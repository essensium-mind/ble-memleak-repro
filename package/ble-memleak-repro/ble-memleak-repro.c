/*
 * "ble-memleak-repro.c"
 *
 * Reproducing a memory leak in the kernel's L2CAP driver.
 *
 * Compilation:
 *
 * - Need the "libbluetooth-dev" package that provides the
 *   "bluetooth/bluetooth.h" and "bluetooth/l2cap.h" headers.
 * - On laptop:
 *     gcc -g -o ble-memleak-repro.amd64 ble-memleak-repro.c
 * - On EPC platform, reusing Yocto's cross-compiler:
 *     CROSSGCC_DIR="/home/ubuntu/fort/ssd-data-big/epc/fort-epc-stm32-04/build-lmp-dev/tmp-lmp_dev/work/cortexa7t2hf-neon-vfpv4-lmp-linux-gnueabi/transient-iface-wrapper/0.6.0-r0"
 *     ${CROSSGCC_DIR}/recipe-sysroot-native/usr/bin/arm-lmp-linux-gnueabi/arm-lmp-linux-gnueabi-gcc -mthumb -mfpu=neon-vfpv4 -mfloat-abi=hard -mcpu=cortex-a7 --sysroot=${CROSSGCC_DIR}/recipe-sysroot -O0 -g -o ble-memleak-repro ble-memleak-repro.c
 *
 * Olivier L'Heureux, 2023-07-10
 * Copyright (c) 2023 FORT Robotics Inc.
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>

int create_bind_connect_socket(void)
{
    int err;

    // socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP) = 19
    int sockfd = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);

    if (sockfd == -1) {
        printf("Error in socket(), code %d: %s\n", errno,
               strerror(errno));
        exit(11);
    }

    // bind(19, {sa_family=AF_BLUETOOTH, l2_psm=htobs(L2CAP_PSM_LE_DYN_START), l2_bdaddr=ea:09:07:a3:22:00, l2_cid=htobs(0 /* L2CAP_CID_??? */), l2_bdaddr_type=BDADDR_LE_PUBLIC}, 14) = 0

    // setsockopt(19, SOL_BLUETOOTH, BT_RCVMTU, "\0\5", 2) = 0
    // setsockopt(19, SOL_SOCKET, SO_SNDBUF, [1024], 4) = 0
    // setsockopt(19, SOL_SOCKET, SO_RCVBUF, [1024], 4) = 0
    // setsockopt(19, SOL_SOCKET, SO_SNDTIMEO_OLD, "\1\0\0\0\0\0\0\0", 8) = 0

    struct timeval timeout;

    timeout.tv_sec =  2;
    timeout.tv_usec = 0;

    err = setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO,
                     &timeout, sizeof(timeout));
    if (err < 0) {
        printf("Error in setsockopt(), code %d: %s\n", errno,
               strerror(errno));
        close(sockfd);
        exit(13);
    }

    // setsockopt(19, SOL_BLUETOOTH, BT_FLUSHABLE, [1], 4) = 0
    // ioctl(19, FIONBIO, [1])           = 0
    // connect(19, {sa_family=AF_BLUETOOTH, l2_psm=htobs(L2CAP_PSM_LE_DYN_START), l2_bdaddr=00:0a:07:a3:22:00, l2_cid=htobs(0 /* L2CAP_CID_??? */), l2_bdaddr_type=BDADDR_LE_PUBLIC}, 14) = -1 EINPROGRESS (Operation now in progress)

    struct sockaddr_l2 ble_addr;

    memset(&ble_addr, 0, sizeof(ble_addr));
    ble_addr.l2_family = AF_BLUETOOTH;
    // L2CAP_PSM_LE_DYN_START == 0x80, see
    // <https://gitlab.com/search?search=L2CAP_PSM_LE_DYN_START&nav_source=navbar&project_id=4770237&group_id=2242924&search_code=true&repository_ref=master>
    ble_addr.l2_psm = htobs(0x80 /* L2CAP_PSM_LE_DYN_START */);
    // l2_bdaddr=00:0a:07:a3:22:00
    ble_addr.l2_bdaddr = *(bdaddr_t*)("\x00\x0a\x07\xa3\x22\x00");
    ble_addr.l2_cid = htobs(0);
    ble_addr.l2_bdaddr_type = BDADDR_LE_PUBLIC;

    err = connect(sockfd, (struct sockaddr*)&ble_addr, sizeof(ble_addr));
    if (err < 0) {
        printf("Error in connect(), code %d: %s\n", errno,
               strerror(errno));
        close(sockfd);
        if (errno == 115 || errno == 111) {
            return errno;
        }
        exit(14);
    }

    close(sockfd);

    return 0;
}

int main(void)
{
    int ret = 0;

    printf("Reproducing a memory leak in the kernel's L2CAP driver.\n");
    printf("(Ctrl-C to interrupt)\n");

    do {
        ret = create_bind_connect_socket();
    } while (ret == 115 || ret == 111);

    return ret;
}
