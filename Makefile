# SPDX-FileCopyrightText: 2023 Arnout Vandecappelle <arnout@mind.be>
#
# SPDX-License-Identifier: GPL-2.0-or-later

BUILDROOT_VERSION = 2023.02.2

# The directory in which this Makefile resides is used as BR2_EXTERNAL
THISDIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
export BR2_EXTERNAL = $(THISDIR)

# Put downloads in this directory instead of in the Buildroot directory
ifeq ($(BR2_DL_DIR),)
BR2_DL_DIR = $(THISDIR)/dl
endif
export BR2_DL_DIR

.PHONY: _all
_all: buildroot/Makefile buildroot/local.mk
	$(MAKE) -C $(THISDIR)/buildroot $(all)

# Clone buildroot if it doesn't exist yet
buildroot/Makefile:
	git clone https://gitlab.com/buildroot.org/buildroot.git -b $(BUILDROOT_VERSION)

buildroot/local.mk: buildroot/Makefile local.mk.in
	sed -e 's%@THISDIR@%$(THISDIR)%g' local.mk.in > $@

all := $(filter-out Makefile buildroot/Makefile,$(MAKECMDGOALS))

.PHONY: $(all)
$(all): _all
	@:

Makefile:;
