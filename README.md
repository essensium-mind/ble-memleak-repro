<!--
SPDX-FileCopyrightText: 2023 Arnout Vandecappelle <arnout@mind.be>
SPDX-FileCopyrightText: 2023 Olivier L'Heureux <olivier.lheureux@mind.be>

SPDX-License-Identifier: GPL-2.0-or-later
-->

Reproduce and Fix a Memory Leak in Linux's Bluetooth Subsystem
==============================================================

Memory Leak Overview
--------------------

We have found a memory leak in the L2CAP layer of Linux's Bluetooth
Subsystem, the same as reported by syzbot in ["*[syzbot] [bluetooth?]
memory leak in hci_conn_add (2)*" (2023-09-02 23:25:00 -0700)][19].

We can reproduce it. When, in a loop, a user-mode program:

 1. Opens a Bluetooth socket at the L2CAP layer:

          sockfd = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);

 2. Set a timeout on the socket:

          timeout.tv_sec  = 2;
          timeout.tv_usec = 0;

          err = setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout));

 3. Connect to a specific Bluetooth address:

          struct sockaddr_l2 ble_addr;

          memset(&ble_addr, 0, sizeof(ble_addr));
          ble_addr.l2_family = AF_BLUETOOTH;
          ble_addr.l2_psm = htobs(0x80 /* L2CAP_PSM_LE_DYN_START */);
          // l2_bdaddr=00:0a:07:a3:22:00
          ble_addr.l2_bdaddr = *(bdaddr_t*)("\x00\x0a\x07\xa3\x22\x00");
          ble_addr.l2_cid = htobs(0);
          ble_addr.l2_bdaddr_type = BDADDR_LE_PUBLIC;

          err = connect(sockfd, (struct sockaddr*)&ble_addr, sizeof(ble_addr));

and when the user program does those three steps in a loop, then the
kernel leaks the [``struct l2cap_conn``][5] and [``struct
hci_conn``][6] objects allocated in [``l2cap_conn_add()``][2] and
[``hci_conn_add()``][3]. Those objects are never freed.

The ``ble-memleak-repro`` program reproduces the memory leak, if the
kernel is not patched. Its source is in
[``package/ble-memleak-repro/ble-memleak-repro.c``][18].

Memory Leak Fix
---------------

We have fixed the memory leak, see the patch series in
``patches/linux/``:

 1. The first patch
    [``0001-ARM-dts-stm32-Add-Bluetooth-usart2-feature-on-stm32m.patch``][11]
    enables Bluetooth on the DK2.
 2. The second patch
    [``0002-Bluetooth-add-many-traces-for-allocation-free-refcou.patch``][12]
    adds many dynamic debug traces that help understand the kernel
    behaviour and freeing problems.
 3. Patches
    [``0003-Bluetooth-L2CAP-use-refcount-on-struct-l2cap_chan-co.patch``][13]
    and
    [``0004-Bluetooth-L2CAP-free-the-leaking-struct-l2cap_conn.patch``][14]
    fix the memory leak.
 4. Patches
    [``0005-Bluetooth-introduce-hci_conn_free-for-better-structu.patch``][15],
    [``0006-Bluetooth-L2CAP-inc-refcount-if-reuse-struct-l2cap_c.patch``][16]
    and
    [``0007-Bluetooth-unlink-objects-to-avoid-use-after-free.patch``][17]
    fixes the use-after-free that appears when the [``struct
    l2cap_conn``][5] and [``struct hci_conn``][6] objects are freed.

The commit messages explain why the commit is needed, which problem
the commit solves, and how.

The first and second patches are present for the memory leak
reproduction only, they should not be part of a final fix.

Patch Status
------------

As of writing, the memory leak is fixed. The fix opened the door to
other problems, especially use-after-free, sometimes followed by
crashes due to NULL dereferences. We think there are weak references
(i.e. pointers that don't increment the refcounter) previously
pointing to memory that was never freed, but now is freed. On kernels
v5.13 and v5.15, patches 0005, 0006 and 0007 seem to fix the
use-after-free and NULL dereferences, but not on kernel v6.5 yet.

Reproducing with Buildroot
--------------------------

We have used [Buildroot][7] to build the kernel, the
``ble-memleak-repro`` test application and create a minimal firmware
that shows the memory leak. This ``ble-memleak-repro`` repository is
an [external Buildroot customisation][8]. Its makefile downloads
Buildroot and configures it for either:

 1. the QEMU versatile platform, or
 2. ST's Discovery Kit [DK2][4] that contains an STM32MP157C, a SoC
    with a dual ARM Cortex-A7, and which supports Bluetooth.

We have experienced a lot of problems to pass a Bluetooth device from
a host to the QEMU-emulated firmware. We could easily reproduce the
memory leak on DK2.

To build the firmware:

 1. Download Buildroot and configure it for either:

    1. QEMU:

             $ make qemu_arm_defconfig

    2. Or DK2:

             $ make stm32mp157c_dk2_defconfig

 2. If you want to run the l2cap-tester:

    Perform the additional setup described below before proceeding to make

 3. Build the firmware:

          $ make

For now, the kernel is configured to upstream 6.5, the latest at the
time of writing.

To build another version present in some linux sources, use
`LINUX_OVERRIDE_SRCDIR` and point it to the kernel source directory.

      $ export LINUX_OVERRIDE_SRCDIR=.../<kernel_source_path>/kernel-source
      $ make

You can point this to a different (cloned) linux source directory,
which makes it easy to experiment with patches.

After building, you'll find the images in `buildroot/output/images/`.

Either start a QEMU, see *Reproducing with QEMU* below, or flash a
DK2's SD-Card and boot it. Log in on the serial console as root, no
password.

To iterate on modifications in the kernel:

* Clone the kernel to some directory and point `LINUX_OVERRIDE_SRCDIR`
  to it.
* After each change, rebuild with `make linux-rebuild world` and
  re-run qemu, or install the new kernel in the ``/boot/`` directory
  on the DK2's SD-Card.
* To change the kernel config, use `make linux-menuconfig`. To persist
  it, use `make linux-update-defconfig`.

Reproducing with the Test Program
---------------------------------

To reproduce the memory leak, or to show that the memory leak is
fixed:

 0. Optional step if you want to see the logging with dmesg

          $ mount -t debugfs none /sys/kernel/debug
          $ echo 'module bluetooth +p' > /sys/kernel/debug/dynamic_debug/control

 1. Record the date and the number of objects in the ``kmalloc-2k`` and
    ``kmalloc-256`` [slabs][10]:

          $ date ; grep -E '^(# name|kmalloc-(4k|2k|1k|512|256|192|128))' /proc/slabinfo

    (On previous kernels, the leaking slabs were the ``kmalloc-1k`` and
    ``kmalloc-192`` slabs, therefore we print the 4 slabs' info.)

 2. Then trig the memory leak: reset the Bluetooth device and start the
    test program:

          $ hciconfig hci0 reset
          $ echo clear > /sys/kernel/debug/kmemleak  # [optional]
          $ ble-memleak-repro

    Every 2 seconds, the test program will normally output "error"
    messages like:

          Error in connect(), code 115: Operation now in progress

    or:

          Error in connect(), code 111: Connection refused

    Those error messages reports the errors of the ``connect()`` call.

    You can stop the test program with Ctrl-C. Usually, 10 error
    messages are enough to show an increase in the slab's sizes.

 3. Record the date and the number of objects in the ``kmalloc-2k`` and
    ``kmalloc-256`` slabs again:

          $ date ; grep -E '^(# name|kmalloc-(4k|2k|1k|512|256|192|128))' /proc/slabinfo
          $ echo scan > /sys/kernel/debug/kmemleak ; cat /sys/kernel/debug/kmemleak  [optional]

    If the number of objects increases linearly with the number of
    ``Error in connect()`` messages, the kernel leaks memory. Else the
    memory leak is fixed. We have measured leaks of +-1720 KiByte/s for
    ``kmalloc-2k`` and +-328 KiByte/s for ``kmalloc-256``.

    It may be necessary to wait a bit for the optional step, or repeat
    it to allow the logs to appear

Running the l2cap-tester
------------------------

The bluez5_utils package provides a set of test tools. For our case,
l2cap-tester is the one we will use to check test coverage for the
memleak.

 1. Enabling the tests to be built

    Before building the image, use buildroot's 'make menuconfig' to
    select all bluez options available in 'target packages'/'network
    applications'.

 2. Configuring the kernel to allow the tests to be run using buildroot's
    'make linux-menuconfig'

    Make sure to include the settings described in:
    https://github.com/bluez/bluez/wiki/test%E2%80%90runner
    (enabling PCI in the kernel is necessary)

 3. Use a version of l2cap-tester that includes timeout tests

    Patches are automatically applied to l2cap-tester to include the
    tests. See the patches/bluez_utils-5.68 dir for details.

The l2cap-tester can be run in the same way as the Test Program to verify
the precence of the memleak.

Reproducing on DK2
------------------

On DK2, flash the SD-Card with ``dd``, as explained in Buildroot's DK2
[``readme.txt``][9]:

      $ sudo dd if=output/images/sdcard.img of=/dev/sdX

Then boot the DK2 and follows the *Reproducing with the Test Program*
above.

Reproducing on QEMU
-------------------

Before running QEMU, make sure the btusb dongle is removed from the host.
Find its location with `lsusb -t`.

```
/:  Bus 04.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/2p, 10000M
/:  Bus 03.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/2p, 480M
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/10p, 10000M
    |__ Port 3: Dev 2, If 0, Class=Mass Storage, Driver=usb-storage, 5000M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/16p, 480M
    |__ Port 1: Dev 20, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 3: Dev 21, If 0, Class=Human Interface Device, Driver=usbhid, 1.5M
        |__ Port 4: Dev 22, If 0, Class=Human Interface Device, Driver=usbhid, 1.5M
    |__ Port 8: Dev 14, If 0, Class=Video, Driver=uvcvideo, 480M
    |__ Port 8: Dev 14, If 1, Class=Video, Driver=uvcvideo, 480M
    |__ Port 9: Dev 16, If 0, Class=Vendor Specific Class, Driver=, 12M
    |__ Port 11: Dev 18, If 0, Class=Chip/SmartCard, Driver=usbfs, 12M
    |__ Port 14: Dev 19, If 0, Class=Wireless, Driver=btusb, 12M
    |__ Port 14: Dev 19, If 1, Class=Wireless, Driver=btusb, 12M
```

In this case it's bus 1, port 14.

Ideally, just unplug the dongle now.
If that is not possible, unbind the device.

```
  $ echo 1-14 | sudo tee /sys/bus/usb/drivers/usb/unbind
```

It no longer appears in lsusb.

Start qemu with passthrough of bus 1, port 14.

```
  $ sudo qemu-system-arm -M versatilepb -audiodev none,id=none -serial stdio \
        -net nic,model=rtl8139 -net user -display none \
        -kernel buildroot/output/images/zImage \
        -dtb buildroot/output/images/versatile-pb.dtb \
        -drive file=buildroot/output/images/rootfs.ext2,if=scsi,format=raw \
        -append "rootwait root=/dev/sda console=ttyAMA0,115200" \
        -device qemu-xhci -device usb-host,hostbus=1,hostport=14
```

(Instead of sudo, it's also possible to configure udev to get
permissions on the usb config filesystem, but I didn't investigate
that.)

Log in on the serial console as root, no password.

Plug in the device again.
I didn't find out how to do that with a non-removable device.
Check that the device is there with lsusb.

Now run the test, see *Reproducing with the Test Program* above.

I don't know how to terminate a running program without terminating qemu.
There's also an ssh daemon running, but I'm not sure how to configure the interface so you can access it from the host.

References:
-----------

- [1]: <https://www.kernel.org/doc/html/v6.5/core-api/kref.html>
       "Adding reference counters (krefs) to kernel objects"
  [1]  "*Adding reference counters (krefs) to kernel objects*"

- [2]: <https://elixir.bootlin.com/linux/v6.5/source/net/bluetooth/l2cap_core.c#L7833>
       "l2cap_conn_add()"
  [2]  "``l2cap_conn_add()``"

- [3]: <https://elixir.bootlin.com/linux/v6.5/source/net/bluetooth/hci_conn.c#L986>
       "hci_conn_add()"
  [3]  "``hci_conn_add()``"

- [4]: <https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-discovery-kits/stm32mp157c-dk2.html>
       "STM32MP157C-DK2 (DK2)"
  [4]  "*STM32MP157C-DK2* (*DK2*)"

- [5]: <https://elixir.bootlin.com/linux/v6.5/source/include/net/bluetooth/l2cap.h#L674>
       "struct l2cap_conn"
  [5]  "``struct l2cap_conn``"

- [6]: <https://elixir.bootlin.com/linux/v6.5/source/include/net/bluetooth/hci_core.h#L685>
       "struct hci_conn"
  [6]  "``struct hci_conn``"

- [7]: <https://buildroot.org/>
       "Buildroot is a tool to generate embedded Linux systems"
  [7]  "*Buildroot is a simple, efficient and easy-to-use tool to
       generate embedded Linux systems through cross-compilation.*"

- [8]: <https://buildroot.org/downloads/manual/manual.html#outside-br-custom>
       "9.2. Keeping customizations outside of Buildroot"
  [8]  "*9.2. Keeping customizations outside of Buildroot*"

- [9]: <https://gitlab.com/buildroot.org/buildroot/-/blob/2023.05.2/board/stmicroelectronics/stm32mp157c-dk2/readme.txt?ref_type=tags>
       "Buildroot's STM32MP157C Discovery Kit v2 'readme.txt'"
  [9]  "Buildroot's STM32MP157C Discovery Kit v2 ``readme.txt``"

- [10]: <https://man7.org/linux/man-pages/man5/slabinfo.5.html>
        "slabinfo(5) — Linux manual page"
  [10]  "slabinfo(5) — Linux manual page"

- [11]: <patches/linux/0001-ARM-dts-stm32-Add-Bluetooth-usart2-feature-on-stm32m.patch>
        "0001-ARM-dts-stm32-Add-Bluetooth-usart2-feature-on-stm32m.patch"
  [11]  "``0001-ARM-dts-stm32-Add-Bluetooth-usart2-feature-on-stm32m.patch``"

- [12]: <patches/linux/0002-Bluetooth-add-many-traces-for-allocation-free-refcou.patch>
        "0002-Bluetooth-add-many-traces-for-allocation-free-refcou.patch"
  [12]  "``0002-Bluetooth-add-many-traces-for-allocation-free-refcou.patch``"

- [13]: <patches/linux/0003-Bluetooth-L2CAP-use-refcount-on-struct-l2cap_chan-co.patch>
        "0003-Bluetooth-L2CAP-use-refcount-on-struct-l2cap_chan-co.patch"
  [13]  "``0003-Bluetooth-L2CAP-use-refcount-on-struct-l2cap_chan-co.patch``"

- [14]: <patches/linux/0004-Bluetooth-L2CAP-free-the-leaking-struct-l2cap_conn.patch>
        "0004-Bluetooth-L2CAP-free-the-leaking-struct-l2cap_conn.patch"
  [14]  "``0004-Bluetooth-L2CAP-free-the-leaking-struct-l2cap_conn.patch``"

- [15]: <patches/linux/0005-Bluetooth-introduce-hci_conn_free-for-better-structu.patch>
        "0005-Bluetooth-introduce-hci_conn_free-for-better-structu.patch"
  [15]  "``0005-Bluetooth-introduce-hci_conn_free-for-better-structu.patch``"

- [16]: <patches/linux/0006-Bluetooth-L2CAP-inc-refcount-if-reuse-struct-l2cap_c.patch>
        "0006-Bluetooth-L2CAP-inc-refcount-if-reuse-struct-l2cap_c.patch"
  [16]  "``0006-Bluetooth-L2CAP-inc-refcount-if-reuse-struct-l2cap_c.patch``"

- [17]: <patches/linux/0007-Bluetooth-unlink-objects-to-avoid-use-after-free.patch>
        "0007-Bluetooth-unlink-objects-to-avoid-use-after-free.patch"
  [17]  "``0007-Bluetooth-unlink-objects-to-avoid-use-after-free.patch``"

- [18]: <package/ble-memleak-repro/ble-memleak-repro.c>
        "ble-memleak-repro.c"
  [18]  "``package/ble-memleak-repro/ble-memleak-repro.c``"

- [19]: <https://lore.kernel.org/linux-bluetooth/0000000000000fd01206046e7410@google.com/T/#u>
        "[syzbot] [bluetooth?] memory leak in hci_conn_add (2)"
  [19]  "[syzbot] [bluetooth?] memory leak in hci_conn_add (2)"
        2023-09-02 23:25:00 -0700

