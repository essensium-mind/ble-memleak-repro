# SPDX-FileCopyrightText: 2023 Arnout Vandecappelle <arnout@mind.be>
#
# SPDX-License-Identifier: GPL-2.0-or-later

include $(BR2_EXTERNAL_BLE_MEMLEAK_REPRO_PATH)/package/*/*.mk
